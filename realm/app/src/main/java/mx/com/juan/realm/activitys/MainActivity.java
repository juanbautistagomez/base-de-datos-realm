package mx.com.juan.realm.activitys;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import mx.com.juan.realm.R;
import mx.com.juan.realm.adapter.BoardAdapter;
import mx.com.juan.realm.models.Board;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity implements RealmChangeListener<RealmResults<Board>>, AdapterView.OnItemClickListener {
    private FloatingActionButton fab;
    private Realm realm;

    private ListView listView;
    private BoardAdapter adapter;
    private RealmResults<Board> boards;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        realm = Realm.getDefaultInstance();
        boards = realm.where(Board.class).findAll();
        boards.addChangeListener(this);

        adapter = new BoardAdapter(this, boards, R.layout.list_view_board_item);
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertCreatingBoard("Titulo", "ingrese el identificador de titulo");
            }
        });

    }

    private void createNewBoard(String boardName) {
        realm.beginTransaction();
        Board board = new Board(boardName);
        /*realm = Realm.getDefaultInstance();*/
        realm.copyToRealm(board);
        realm.commitTransaction();

      /*  realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

            }
        });*/

    }

    private void showAlertCreatingBoard(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (title != null) builder.setTitle(title);
        if (message != null) builder.setMessage(message);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_creating_board, null);
        builder.setView(view);
        final EditText editText = (EditText) view.findViewById(R.id.editTextnewBoard);
     /* final EditText editText1 = (EditText)view.findViewById(R.id.alias);*/
        builder.setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String boardName = editText.getText().toString().trim();
                /*String alias = editText.getText().toString().trim();*/
                if (boardName.length() > 0) {
                    createNewBoard(boardName);
                } else {
                    Toast.makeText(getApplicationContext(), "no ha ingresado nada", Toast.LENGTH_SHORT).show();
                }
            }

        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    public void onChange(RealmResults<Board> boards) {
       adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(MainActivity.this, NoteActivity.class);
        intent.putExtra("id",boards.get(position).getId());
        startActivity(intent);

        Toast.makeText(getApplicationContext(), "funciona"+boards.get(position).getId(), Toast.LENGTH_SHORT).show();


    }
}
