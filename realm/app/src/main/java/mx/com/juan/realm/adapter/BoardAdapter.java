package mx.com.juan.realm.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import mx.com.juan.realm.R;
import mx.com.juan.realm.models.Board;

public class BoardAdapter extends BaseAdapter  {


    private Context context;
    private List<Board> list;
    private int layout;

    public BoardAdapter(Context context, List<Board> boards, int layout) {
        this.context = context;
        this.list = boards;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Board getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder vh;
        if (convertView == null) {
            vh = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(layout, null);
            vh.title = (TextView) convertView.findViewById(R.id.title2);
            vh.note = (TextView) convertView.findViewById(R.id.note);
            vh.createAd = (TextView) convertView.findViewById(R.id.date);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        Board board = list.get(i);
        vh.title.setText(board.getTitle());

        int numberOfnotes = board.getNotes().size();
        String textForNotes = (numberOfnotes == 1) ? numberOfnotes + "note" : numberOfnotes + "  Notas";
        vh.note.setText(textForNotes);
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String createdAt = df.format(board.getCreatedAt());
        vh.createAd.setText(createdAt);
        return convertView;
    }

    public class ViewHolder {
        TextView title;
        TextView note;
        TextView createAd;
    }

}
